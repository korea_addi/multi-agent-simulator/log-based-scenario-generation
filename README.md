# Log based scenario generation

Welcome to Log based scenario generation, your go-to solution for log-based scenario generation in the world of automated driving vehicles.

## What is Log based scenario generation?

Log based scenario generation is a powerful tool designed to transform raw driving data into meaningful OpenSCENARIO files. By seamlessly collecting logs from automated driving vehicles, Log based scenario Generation opens the door to a new era of scenario generation for testing and simulation.

## Features

- **Coordinate System Transformation**: Supports multiple coordinate systems including:
  - EPSG:32652 (UTM Zone 52N)
  - EPSG:4326 (WGS84)
  - Custom OpenDRIVE coordinate system

- **Data Processing**:
  - Ego Vehicle Data: Processes position, orientation, and velocity information
  - Obstacle Data: Handles various types of obstacles with classification
  - Time Synchronization: Maintains consistent timing across all data sources

- **Object Classification**:
  - Unknown
  - Unknown Movable
  - Unknown Unmovable
  - Pedestrian
  - Bicycle
  - Vehicle

## Input Data Format

### Ego Vehicle Data (CSV)
The system expects ego vehicle data in CSV format with the following fields:
- Timestamp
- Latitude/Longitude
- Height
- Velocity (North, East, Up)
- Orientation (Roll, Pitch, Azimuth)
- Status

### Obstacle Data (CSV)
Obstacle data should be provided in CSV format containing:
- Timestamp
- Object ID
- Position (X, Y, Z)
- Theta (Orientation)
- Velocity
- Size
- Tracking Time
- Object Type
- Acceleration
- Sub-type
- Height Above Ground

## Output Format

The system generates two main JSON files:
1. **Ego Info JSON**: Contains ego vehicle trajectory data
2. **Object Info JSON**: Contains obstacle trajectory data

## Dependencies

- pyproj
- logging
- csv
- math
- json

## Notes

- The system uses a specific geo-reference format compatible with OpenDRIVE
- Time synchronization starts from the first valid timestamp
- Invalid data rows are automatically skipped with warning messages
- Default object filtering is set to vehicles only, but can be modified

## Contributing

Feel free to contribute to this project by submitting issues or pull requests. All contributions are welcome!

## License

This project is licensed under the GNU Lesser General Public License v3.0 (LGPL-3.0).